package kz.company.lesson_3

import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

private const val COUNT_PREF = "COUNT_PREF"
private const val LIST_PREF = "LIST_PREF"

class MainActivity : AppCompatActivity() {

    private var count = 0
    private var prefs: SharedPreferences? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        prefs = PreferenceManager.getDefaultSharedPreferences(this)

        countButton.setOnClickListener {
            count++
            resultTextView.text = "Count: $count"
        }

        count = prefs?.getInt(COUNT_PREF, 0) ?: 0
        listTextView.text = loadList().toString()
        resultTextView.text = "Count: $count"
    }

    private fun saveList(){
        val editor = prefs?.edit()
        val arr = mutableSetOf<String>()
        arr.add("Azat")
        arr.add("Azamat")
        arr.add("Abzal")
        editor?.putStringSet(LIST_PREF, arr)
        editor?.apply()
    }

    private fun loadList() =
        prefs?.getStringSet(LIST_PREF, emptySet())

    override fun onDestroy() {
        val editor = prefs?.edit()
        editor?.putInt(COUNT_PREF, count)
        editor?.apply()
        saveList()
        super.onDestroy()
    }
}
